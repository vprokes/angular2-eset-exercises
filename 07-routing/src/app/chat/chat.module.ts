import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChatDetailComponent } from './chat-detail/chat-detail.component';
import { ChatMessagesComponent } from "./chat-messages/chat-messages.component";
import { ChatFormComponent } from "./chat-form/chat-form.component";
import { RoomsComponent } from "./rooms/rooms.component";
import { ChatComponent } from './chat.component';
import { ChatRoutingModule } from "./chat-routing.module";
import { ReactiveFormsModule } from "@angular/forms";
import { RoomInfoComponent } from "./room-info/room-info.component";
import { ChatService } from './chat.service';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    ChatRoutingModule
  ],
  declarations: [
    RoomsComponent,
    RoomInfoComponent,
    ChatFormComponent,
    ChatMessagesComponent,
    ChatDetailComponent,
    ChatComponent
  ],
  exports: [
    ChatComponent
  ],
  providers: [ChatService]
})
export class ChatModule {
}
