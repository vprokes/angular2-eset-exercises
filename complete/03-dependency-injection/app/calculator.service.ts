import { Injectable, Optional } from '@angular/core';
import { Book } from './model/book';

@Injectable()
export class CalculatorService {

  constructor() { }

  calculate(book: Book) {
    const colorPagePrice = 10;
    const normalPagePrice = 1;
    const letterOnCoverPrice = 2;

    let price = 0;

    const normalPages = book.totalPages - book.colorPages;

    price += book.title.length * letterOnCoverPrice;
    price += normalPages * normalPagePrice;
    price += book.colorPages * colorPagePrice;

    return price;
  }
}

