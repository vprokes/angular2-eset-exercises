import { Component, OnInit } from '@angular/core';
import { ChatService } from "./chat.service";
import { Observable } from 'rxjs';
import { Room } from './model/chat';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html'
})
export class ChatComponent implements OnInit {

  rooms$ : Observable<Room[]>;
  constructor(private chatService : ChatService) { }

  ngOnInit() {
    this.rooms$ = this.chatService.getRooms();
  }

}
