import { Component, OnInit, Input } from '@angular/core';
import { Address } from '../model/order';

@Component({
  selector: 'app-order-address',
  templateUrl: './order-address.component.html'
})

export class OrderAddressComponent implements OnInit {

  @Input() title = "Address";
  @Input() address: Address;

  constructor() { }

  ngOnInit() { }

}
